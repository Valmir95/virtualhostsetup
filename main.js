$(document).ready(function() {
    $("#virtual_host_form").on('submit', function (e) {
        e.preventDefault();
        var serverNameInput = $("#server_name");
        var documentRootInput = $("#document_root");
        var portInput = $("#port");
        $.ajax({
            type:'POST',
            url:'virtualhost.php',
            data:{'serverName':serverNameInput.val(), 'documentRoot': documentRootInput.val(), 'port': portInput.val()},
            success: function(data){
                if(isNaN(data)){
                    alert("Something went wrong");
                }
                else {
                    alert("Virtualhost successfully added!");
                }
            }

        });
    });
});