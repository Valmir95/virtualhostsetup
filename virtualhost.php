<?php
/**
 * Created by PhpStorm.
 * User: valmemet
 * Date: 01/12/2017
 * Time: 14:03
 */

//Hardcoded as fuck. May use some fancy php method to get location of php and go back n-directories bla bla bla,
//but don't give a fuck, because script is (at the moment) only for me. Skrrraa.
$vHostPath = getVhostPath();
$hostsPath = getHostsPath();



if(isset($_POST['serverName']) && isset($_POST['documentRoot'])){
    return addVirtualHostApache($hostsPath, $vHostPath);
}
else if(php_sapi_name() === 'cli'){
    addVirtualHostCLI($hostsPath, $vHostPath);
}


function addVirtualHostApache($hostsPath, $vHostPath){
    $port = "80";
    $serverName = $_POST['serverName'];
    $documentRoot = $_POST['documentRoot'];
    if(isset($_POST['port']) && !empty($_POST['port'])){
        $port = $_POST['port'];
    }
    //Default as of now, as this script is only made for me.
    $virtualHostString = getVirtualHostString($serverName, $documentRoot, $port);

    $hostsString = getHostsString($serverName);

    return addVhostAndHost($hostsPath, $hostsString, $vHostPath, $virtualHostString);
}

function addVirtualHostCLI($hostsPath, $vHostPath){
    while(!file_exists($vHostPath)){
        echo "The path to XAMPP was not found. \n";
        $vHostPath = readline("Please specify the path to XAMPP");
        if(substr($vHostPath, -1) != '/'){
            $vHostPath .= '/';
        }
        if (strpos($vHostPath, 'xamppfiles') !== false) {
            $vHostPath .= "xamppfiles/etc/extra/httpd-vhosts.conf";
        }
        else{
            $vHostPath .= 'apache/conf/extra/httpd-vhosts.conf';
        }

    }
    if (PHP_OS == 'WINNT') {
        echo "Choose VirtualHost server name:";
        $serverName = stream_get_line(STDIN, 1024, PHP_EOL);
        echo "Add path to your project root:";
        $documentRoot = stream_get_line(STDIN, 1024, PHP_EOL);
        echo "Choose what port to run at (default 80):";
        $port = stream_get_line(STDIN, 1024, PHP_EOL);
    }

    else {
        $serverName = readline("Choose VirtualHost server name:");
        $documentRoot = readline("Add path to your project root:");
        $port = readline("Choose what port to run at (default 80):");
    }
    if(empty($port)){
        $port = "80";
    }
    echo $vHostPath . "\n";
    echo $hostsPath . "\n";

    $virtualHostString = getVirtualHostString($serverName, $documentRoot, $port);
    $hostsString = getHostsString($serverName);

    $addVhostResponse = addVhostAndHost($hostsPath, $hostsString, $vHostPath, $virtualHostString);


    if($addVhostResponse){
        echo "Virtual host successfully added!\n";
    }
    else{
        echo "Failed. Probably your file permissions to the files that are fucked. \n";
    }
    return;
}

function getVhostPath($os = ''){
    //TODO: Return this for now. Will later implement dynamic finding based on OS
    $defaultPathWindows = "C:/xampp/apache/conf/extra/httpd-vhosts.conf";
    $defaultPathMac = "/Applications/XAMPP/xamppfiles/etc/extra/httpd-vhosts.conf";
    if (file_exists($defaultPathWindows)) {
        return $defaultPathWindows;
    }
    return $defaultPathMac;
}

function getHostsPath($os = ''){
    //TODO: Return this for now. Will later implement dynamic finding based on OS
    $defaultPathWindows = "C:/Windows/System32/drivers/etc/hosts";
    $defaultPathMac = "/private/etc/hosts";
    if (file_exists($defaultPathWindows)) {
        return $defaultPathWindows;
    }
    return $defaultPathMac;
}

/**
 * @param $hostsPath
 * @param $hostsString
 * @param $vHostPath
 * @param $virtualHostString
 * @return false|int
 * Adds given values inside Vhosts and hosts file. Will return amount of bytes on success, and false on
 */
function addVhostAndHost($hostsPath, $hostsString, $vHostPath, $virtualHostString){
    $hostsPutContent = file_put_contents($hostsPath, $hostsString, FILE_APPEND);
    $vhostsPutContent =  file_put_contents($vHostPath, $virtualHostString, FILE_APPEND);
    if(is_numeric($hostsPutContent) && is_numeric($vhostsPutContent)){
        return $hostsPutContent + $vhostsPutContent;
    }
    else{
        return false;
    }
}

//TODO: Use port specified by user.
function getVirtualHostString($serverName, $documentRoot, $port){
    return "

<VirtualHost *:$port>
    ServerName $serverName
    DocumentRoot \"$documentRoot\"
    <Directory \"$documentRoot\">
        Options +Indexes +Includes +FollowSymLinks +MultiViews
        AllowOverride All
        Order Deny,Allow
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>";
}

function getHostsString($serverName){
    return "
127.0.0.1 $serverName";
}




